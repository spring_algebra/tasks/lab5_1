/*
 * Algebra labs.
 */

package com.example.demo.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.demo.config.SpringConfig;
import com.example.demo.service.Catalog;

public class UT_Catalog {

	@Test
	public void catalogTest() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		assertTrue("spring container should not be null", ctx != null);

		Catalog catalog = ctx.getBean(Catalog.class);
		
		System.out.println("Catalog size: " + catalog.size());
		System.out.println(catalog.findById(1L));

		ctx.close();
	}

}

