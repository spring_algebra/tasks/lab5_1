/*
 * Algebra labs.
 */

package com.example.demo.aspects;

// TODO: Declare as an aspect
public class CatalogAspect {

	// TODO: Declare the below as a pointcut expression, with name anyCatalogMethod 
	@TODO("execution(* com.example.demo.service.Catalog.*(..))")
	public void TODO() {} // No body in this method
	
	
	// TODO: Add before advice associated with anyCatalogMethod pointcut
	public void dbPing() {
		System.out.println("Pinging database ...");
	}

}