/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

// Declare as a Spring configuration class
@Configuration
// TODO: Enable AspectJ support

@Import({SpringAspectsConfig.class, SpringRepositoryConfig.class, SpringServicesConfig.class})
public class SpringConfig {}